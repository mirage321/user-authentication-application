import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {

  username: string;
  password: string;
  url: string ="http://localhost:9000/users";
  failureMessage: string = '';

  constructor(private _http: HttpClient, private _router: Router) { }

  ngOnInit() {
  }

  validateLoginCredentials(){
    var url = `${this.url}/${this.username}/${this.password}`;
    this._http.get<{'message': string}>(url).subscribe(x => {
      console.log(x);
      if(x.message == 'User located') {
        console.log('Login Sucessful');
        localStorage.setItem('username',this.username);
        this._router.navigate(['./dashboard']);
      }
      else {
        console.log('Login is not sucessful');
        this.failureMessage = 'invalid User credentials';
      }
    });
  }
}
