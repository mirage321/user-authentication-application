import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../user';

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css']
})
export class UserRegisterComponent implements OnInit {

  message: string = '';
  url: string = 'http://localhost:9000/users';
  user: User = new User();
  username: string;
  password: string;
  email: string;


  constructor(private _http: HttpClient) { }

  ngOnInit() {
  }

  registerUser(){
    this.user.username = this.username;
    this.user.password = this.password;
    this.user.email = this.email;

    this._http.post(this.url, this.user,{
      headers: new HttpHeaders({"Content-type":"application/json"})
    }).subscribe(x => {
      console.log(x);
      this.message = 'User Registration sucessful';
      this.clearAll();
    })
  }

  clearAll(){
    this.user = new User();
  
  }
}
