import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserLoginContainerComponent } from '../user-login-container/user-login-container.component';
import {UserDashboardComponent} from '../user-dashboard/user-dashboard.component';
import {UserLoginComponent} from '../user-login/user-login.component';
import {UserLogoutComponent} from '../user-logout/user-logout.component';

const routes: Routes = [
  {path:'container', component: UserLoginContainerComponent},
  {path:'dashboard', component: UserDashboardComponent},
  {path:'logout', component: UserLogoutComponent},
  {path:'login', component: UserLoginComponent},

  {path: '', redirectTo: '/container', pathMatch: 'full'}
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
