import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.css']
})
export class UserDashboardComponent implements OnInit {

  message: string = '';

  constructor() { 
    if (localStorage.getItem('username') != null){
      this.message = `Hello ${localStorage.getItem('username')}, Welcome to your dashboard`
    }
  }

  ngOnInit() {
  }

}
